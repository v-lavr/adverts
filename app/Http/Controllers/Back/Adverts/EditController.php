<?php

namespace App\Http\Controllers\Back\Adverts;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Back\Adverts\SaveController;
use Illuminate\Support\Facades\Validator;
use App\Model\Advert;

class EditController extends Controller
{
    protected $request;

    protected $saveController;

    protected $advertId;

    /**
     * EditController constructor.
     * @param Request $request
     * @param \App\Http\Controllers\Back\Adverts\SaveController $saveController
     */
    public function __construct(
        Request $request,
        SaveController $saveController
    ) {
        $this->request = $request;
        $this->saveController = $saveController;
    }

    /**
     * @param Advert $advert
     * @param null $id
     * @return $this|\Illuminate\Contracts\View\Factory|RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function execute(Advert $advert, $id = null)
    {
        $this->advertId = $id;
        if ($this->request->isMethod('post')) {
            $data = $this->prepareData();
            $validator = $this->validator($data);
            if ($validator->fails()) {
                return redirect()->route('edit')->withErrors($validator)->withInput();
            }
            $this->saveAdvert($data);
            return redirect('/' . $this->advertId);
        } elseif ($this->request->isMethod('get') && $advert->find($id)) {
            $advertData = $advert->find($id)->toArray();
            $data = [
                'id' => $id,
                'title' => $advertData['title'],
                'data' => $advertData
            ];
            return view('template.pages.edit-adverts', $data);
        }

        return view('template.pages.edit-adverts');
    }

    /**
     * @param $data
     * @return bool
     */
    protected function saveAdvert($data)
    {
        $this->advertId = $this->saveController->save($data);
        return true;
    }

    /**
     * @return array
     */
    protected function prepareData()
    {
        $data = $this->request->except('_token');

        $data += [
            'id' => $this->advertId,
            'author' => Auth::user()->name,
            'author_id' => Auth::user()->id
        ];

        return $data;
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:100',
            'description' => 'required',
        ]);
    }
}
