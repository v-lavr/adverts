<?php

namespace App\Http\Controllers\Back\Adverts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Advert;

class SaveController extends Controller
{
    protected $advert;

    /**
     * SaveController constructor.
     * @param Advert $advert
     */
    public function __construct(
        Advert $advert
    ) {
        $this->advert = $advert;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function save($data)
    {
        if (isset($data['id'])) {
            $this->advert->find((int)$data['id'])->update([
                'title' => $data['title'],
                'description' => $data['description'],
            ]);
            $result = $data['id'];
        } else {
            $this->advert->fill($data);
            $this->advert->save();
            $result = $this->advert->id;
        }
        return $result;
    }

}
