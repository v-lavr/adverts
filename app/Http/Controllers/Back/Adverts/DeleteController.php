<?php

namespace App\Http\Controllers\Back\Adverts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Advert;

class DeleteController extends Controller
{
    /**
     * @param Advert $advert
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function execute(Advert $advert, Request $request)
    {
        $advertModel = $advert->find($request->id);
        if ($advertModel) {
            $advertModel->delete();
        }
        return redirect('/');
    }
}
