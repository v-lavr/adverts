<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Advert;

class IndexController extends Controller
{
    protected $request;

    protected $advertId;

    /**
     * IndexController constructor.
     * @param Request $request
     * @param Advert $advert
     */
    public function __construct(
        Request $request,
        Advert $advert
    ) {
        $this->request = $request;
        $this->advert = $advert;
    }

    /**
     * @return $this|void
     */
    public function execute()
    {
        if ($this->hasAdvertId()) {
            $data = $this->getAdvertData($this->advertId);
            if (is_array($data)) {
                return view('template.pages.view-advert')->with('advert', $data);
            } else {
                return abort(404);
            }
        }

        return view('template.index')->with('adverts', $this->advert->paginate(5));
    }

    /**
     * @return bool
     */
    protected function hasAdvertId()
    {
        if ((int)$this->request->id) {
            $this->advertId = (int)$this->request->id;
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return array|bool
     */
    protected function getAdvertData($id)
    {
        $advert = $this->advert->find($id);
        if ($advert) {
            return $advert->attributesToArray();
        } else {
            return false;
        }
    }
}
