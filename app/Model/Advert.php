<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{
    protected $fillable = ['title', 'description', 'author', 'author_id'];
}
