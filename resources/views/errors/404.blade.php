@extends('layouts.blank.1-column')
@section('content')
    <div class="page-404 mt-5 p-5">
        <h1>Page not found!</h1>
    </div>
@endsection