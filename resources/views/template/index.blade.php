@extends('layouts.blank.1-column')
@section('content')
    @if(isset($adverts) && (bool)count($adverts))
        <div class="container">
            <div class="row">
                @foreach($adverts as $item)
                    <div class="col-sm-4">
                        <div class="card text-center p-2 m-2" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a href="{{ route('home', ['id' => $item['id']]) }}">{{ $item['title'] }}</a>
                                </h5>
                                <p class="card-text">{{ $item['created_at'] }}</p>
                                <a href="{{ route('home', ['id' => $item['id']]) }}" class="btn btn-primary">Review</a>
                                @if(Auth::check())
                                    <a href="{{ route('edit', ['id' => $item['id']]) }}"
                                       class="btn btn-secondary">Edit</a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if($adverts->lastPage() > 1)
                <div class="text-center">
                    <div class="d-inline-block mt-5">
                        {{ $adverts->links('template.elements.pagination') }}
                    </div>
                </div>
            @endif
        </div>
    @else
        <div class="container">
            <h2>No added adverts</h2>
            @if(Auth::check())
                <a href="{{ route('edit') }}" >Create Ad</a>
            @else
                <p><a href="{{ route('login') }}" >Login</a> to add them</p>
            @endif

        </div>
    @endif
@endsection