@extends('layouts.blank.1-column')
@section('content')
    @if(isset($advert))
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="title border-bottom">
                    <h3>{{ $advert['title'] }}</h3>
                </div>
                <div class="description mt-4">
                    {{ $advert['description'] }}
                </div>
                <div class="border-top mt-4">
                    <div class="row text-center">
                        <div class="col-9">
                        </div>
                        <div class="col-3 text-right pt-3">
                            <p>Author: <span class="font-weight-bold">{{ $advert['author'] }}</span></p>
                            <p>Created at: <span class="font-weight-bold">{{ $advert['created_at'] }}</span></p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-2">
                @if(Auth::check())
                    @if(Auth::user()->id == $advert['author_id'])
                        <form action="{{ route('delete', ['id' => $advert['id']]) }}" method="DELETE">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    @endif
                    <a href="{{ route('edit', ['id' => $advert['id']]) }}" class="btn btn-secondary mt-2">Edit</a>
                @endif
            </div>
        </div>
    @endif
@endsection