@extends('layouts.blank.1-column')
@section('content')
    <div class="container">
        @if ($errors->any())
            {!! implode('', $errors->all('<p>:message</p>')) !!}
        @endif
        <form action="{{ route('edit') }}{{ (isset($id) ? '/'.$id : null) }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"
                       value="{{ $data['title'] or old('title') }}">
                <small id="titleHelp" class="form-text text-muted">Enter your title.</small>
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" name="description"
                          rows="3"
                          placeholder="Enter description">{{ $data['description'] or old('description') }}</textarea>
                <small id="descriptionHelp" class="form-text text-muted">Enter your description.</small>
            </div>
            <button type="submit" class="btn btn-primary">{{ (isset($id) ? 'Update' : 'Create') }}</button>

        </form>
        <div class="pt-2">
            @if(isset($id) && Auth::user()->id == $data['author_id'])
                <form action="{{ route('delete', ['id' => $id]) }}" method="DELETE">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            @endif
        </div>
    </div>
@endsection
