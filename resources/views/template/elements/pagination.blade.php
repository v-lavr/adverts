<ul class="pagination">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <li class="disabled p-2 mr-2"><span>Previous</span></li>
    @else
        <li class="p-2 mr-2"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a></li>
    @endif

<!-- Pagination Elements -->
    @foreach ($elements as $element)
    <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <li class="disabled"><span>{{ $element }}</span></li>
        @endif

    <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active"><a class="page-link disabled" href="{{ $url }}">{{ $page }}</a>
                    </li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach

<!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <li class="p-2 ml-2"><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a></li>
    @else
        <li class="disabled p-2 ml-2"><span>Next</span></li>
    @endif
</ul>