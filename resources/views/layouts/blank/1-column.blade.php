@include('layouts.header.head')
@include('layouts.header.navbar')
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @yield('head')
    </head>
    <body>
        <div class="header">
            @yield('navbar')
        </div>

        <div class="content ">
            <div class="container-fluid mt-5 p-2">
                @section('content')

                @show
            </div>
        </div>

        <div class="footer">

        </div>
    </body>
</html>