@section('navbar')
    <nav class="navbar navbar-expend-lg navbar-dark bg-dark">
        <a href="{{ route('home') }}" class="navbar-brand">Adverts</a>
        <ul class="nav">
            @if (Auth::guest())
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item h4">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#auth-popup">
                            Login
                        </button>
                    </li>
                </ul>
            @else
                <li class="nav-item">
                    <a href="{{ route('edit') }}" class="nav-link" style="color: cyan">Create Ad</a>
                </li>
                <div class="dropdown">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form class="form-inline" id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            @endif
        </ul>
    </nav>
    <!-- Modal -->
    <div class="modal fade" id="auth-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="text-center">
                    @if (isset($errors) && $errors->any())
                        <script>$('#auth-popup').modal('show'); </script>
                        <div class="alert alert-warning" role="alert">
                            {!! implode('', $errors->all('<p>:message</p>')) !!}
                        </div>
                    @endif
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <label for="name">User Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="User name" required>

                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="User name" required>

                        <div class="modal-footer">
                            <button type="submit" name="" value="login" class="btn btn-primary">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
