<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Not authorized
 */
Route::group(['middleware' => 'web'], function () {

    Route::match(['get', 'post'], '/{id?}', [
        'uses' => 'Front\IndexController@execute',
        'as' => 'home'
    ])->where('id', '[0-9]+');

    Auth::routes();
});


/*
 * Authorized users
 */
Route::group(['middleware' => 'auth'], function () {

    Route::match(['get', 'post'], '/edit/{id?}', [
        'uses' => 'Back\Adverts\EditController@execute',
        'as' => 'edit'
    ])->where('id', '[0-9]+');

    Route::get('/delete/{id}', [
        'uses' => 'Back\Adverts\DeleteController@execute',
        'as' => 'delete'
    ])->where('id', '[0-9]+');
});
Auth::routes();


